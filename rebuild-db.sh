#!/bin/bash

cd ./laravel-project

echo "env received: $1"
env="--env=local"
if [ -n "$1" ]; then
	env=$1
fi

echo "Using $env"

php artisan db:create $env
php artisan migrate:fresh $env
php artisan db:seed $env
